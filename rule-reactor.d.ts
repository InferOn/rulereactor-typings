// Type definitions for [RuleReactor]
// Project: [https://github.com/anywhichway/rule-reactor]
// Definitions by: [Inferon] <[http://stackoverflow.com/users/248703/inferon]>
// Definitions: https://github.com/DefinitelyTyped/DefinitelyTyped

/** RuleReactor is a a light weight, fast, expressive forward chaining business rule engine leveraging JavaScript internals,
 * lazy cross-products, and Functions as objects rather than Rete algorithm approach used in OPS, Clips, Drools, and nools.js.
 */
declare class RuleReactor {
    /**
     * @param name Rules start with a name, 
     * @param salience which is followed by a salience, a.k.a. priority. This is used to decide which rule to execute first if more than one rule has its conditions satisfied. The higher the salience, the higher the priority. Values can run from -Infinity to Infinity.
     * @param domain After this comes the domain specification, which consists of an object the properties of which are the names for class referencing variables that will be used by rule conditions and actions. The values of these properties are the constructors for the classes.
     * @param condition After the domain comes a condition or array of conditions. These are functions that return true or false. They should not produce any side effects because they will be called a lot and strange things might happen as a result. Note, it is generally better to use == rather than === in rules because this allows objects to resolve using their valueOf() function.
     * @param action Finally, an action is specified. This can be a function that executes any normal JavaScript code.
     */
    createRule<T>(name: string, salience: number, domain: T, condition: <T>(scope: T) => boolean | (<T>(scope: T) => boolean)[], action: <T>(scope: T) => void);

    /**
     * Feed data into the reactor
     * @param instances 
     * @param callback
     */
    assert<T>(instances: T | (T)[], callback?: () => void);

    /**
     * Run the reactor
     * @param {number} max The first argument to run is the number of rules to execute before stopping. Unless debugging is being conducted, this is typically set to Infinity.
     * @param {boolean} loose The second argument tells the RuleReactor to run just one rule every time slice to allow the JavaScript engine to handle other requests. By default, this is false since it can have a substantial impact on RuleReactor performance. However, if you have a set of rules that take more than a second to process in production, you should set it to true.
     * @param callback Callback that will be invoked when there are no more rules to process.
     */
    run(max: number, loose: boolean, callback: () => void);

    /**  RuleReactor has 3 trace levels that print to the JavaScript console. Prints when:
     * 1. a) RuleReactor is starting to run b) A specific rule is firing
     * 2. a) A rule is being activated when all it conditions have been met b) A rule is being de-activated when its conditions are no longer being met or immediately after executing its action
     * 3. a) A new rule is being created b) New data is being inserted into RuleReactor c) Data is being bound or unbound from rule. This immediately follows insert for all impacted rules. d) An object with an impact on a rule is being modified. e) A rule is being tested. This happens when at least one of every object in the domain for the rule is bound. It will repeat if a relevant property is changed on a bound object. f) An object is being removed from the RuleReactor.
     * @param {number} level
     */
    trace(level: number): void;

    /**
     * Stop the reactor
     */
    stop(): void;

    /**
     * RuleReactor is a a light weight, fast, expressive forward chaining business rule engine leveraging JavaScript internals,
     * lazy cross-products, and Functions as objects rather than Rete algorithm approach used in OPS, Clips, Drools, and nools.js.
     * It can be used on both the client and the server.
     * @param domain The domain argument is just a hint regarding what classes to augment. It is an object that should have keys by the same name as class constructors you wish to use, e.g. {Person: Person, Home: Home}. If you have any anonymous constructors you will be using, this allows you to effectively de-anonymize them for RuleReactor.
     * @param boost When the boost is set to true, then an extra compile step is taken when creating rules. This can boost performance by up to 25%; however, breakpoints set in rule conditions, will no longer work. So, this should be a final step in development. If you don't want to use the domain argument when using boost, you can set it to null or {}.
     */
    constructor(domain?: any, boost?: boolean);
}

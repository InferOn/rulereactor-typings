# README #

RuleReactor is a a light weight, fast, expressive forward chaining business rule engine leveraging JavaScript internals, lazy cross-products, and Functions as objects rather than Rete algorithm approach used in OPS, Clips, Drools, and nools.js. It can be used on both the client and the server.

All rule conditions and actions are expressed as regular JavaScript functions so a JavaScript debugger can be fully utilized for debugging RuleReactor applications. At 45K (21K minified) vs 577K (227K minified) for Nools, a comparable speed for most applications, plus a low memory impact pattern and join processor, RuleReactor is perfect for memory constrained apps like those on mobile devices.

### What is this repository for? ###

* Basic typing informations for Typescript user